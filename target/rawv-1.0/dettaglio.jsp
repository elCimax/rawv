<%@page import="com.ads.rawv.dto.Giornata"%>
<%@page import="java.util.List"%>
<%@page import="com.ads.rawv.dto.Sintesi"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<script type="text/javascript" src="js/jquery.min.js"></script>
	    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	    <link href="css/rawv.css" media="all" rel="stylesheet" type="text/css" />
	    <link rel='stylesheet' href='css/bootstrap.min.css'>
	</head>
	<body>
		 <% List<Giornata> giornate=(List<Giornata>)session.getAttribute("giornate"); 
		  %>
	    <div class="table-responsive">
	  	 	<div>
				<div style="float:right;"><a class="btn btn-default" href="index.jsp" role="button">Indietro</a></div>
				<div style="float:left;"><h2>${param.nominativo}</h2></div>
			</div>
		    <table class="table table-hover dettaglio">
		    	<thead>
		    		<tr class="active">
		    			<th>Sett.</th>
		    			<th>Giorno</th>
						<th>CDL</th>
						<th>Ore</th>
						<th>Tk</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<c:forEach var="item" items="${giornate}">
		    		<c:if test="${param.nominativo eq item.nominativo}">
					<tr>
						<td>
							<c:if test="${not empty item.week}">
								W${item.week}
							</c:if>
						</td>
						<td>
							${item.giorno}
						</td>
						<td>
						<c:choose>
						 	<c:when test="${not empty item.cdl}">
								${item.cdl}
							</c:when>
						 	<c:when test="${not empty item.assenza}">
								${item.assenza}
							</c:when>
							<c:otherwise>
								-
							</c:otherwise>
						</c:choose>
						</td>
						<td>${item.ore}</td>
						<td>${item.ticket}</td>
					</tr>
					</c:if>
		    		</c:forEach>
		    	</tbody>
		    </table>
	    </div>
	</body>
</html>

<%@page import="java.util.List"%>
<%@page import="com.ads.rawv.dto.Sintesi"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link href="css/rawv.css" media="all" rel="stylesheet" type="text/css" />
    <link rel='stylesheet' href='css/bootstrap.min.css'>
</head>
<body>
	 <% List<Sintesi> sintesi=(List<Sintesi>)session.getAttribute("sintesi"); %>
    <div class="table-responsive">
    <table class="table table-hover">
    	<thead>
    		<tr class="active">
    			<th></th>
    			<th>Ore</th>
    			<th>Giorni</th>
    		</tr>
    	</thead>
    	<tbody>
    		<c:forEach var="item" items="${sintesi}">
    			<tr class="success">
    				<td colspan="3"><a href="dettaglio.jsp?nominativo=${item.nominativo}">${item.nominativo}</a></td>
    			</tr>
    			<c:set var="total" value="0" />
    			<c:forEach var="cdl" items="${item.cdls}">
	    			<tr>
	    				<td>${cdl.key}</td>
	    				<td>${cdl.value}</td>
	    				<td>${cdl.value/8}</td>
	    			</tr>
	    			<c:set var="total" value="${total + cdl.value}" />
    			</c:forEach>
    				<tr class="warning">
	    				<td><span class="bold">Totale:</span></td>
	    				<td>${total}</td>
	    				<td>${total/8}</td>
	    			</tr>
	    			<tr class="active">
						<td colspan="3"></td>
					</tr>
    		</c:forEach>
    	</tbody>
    </table>
    </div>
        <div>
    	<a class="btn btn-default" href="index.jsp" role="button">Carica Ore</a>
    	<a class="btn btn-default" href="reset.jsp" role="button">Cancella Cache</a>
    </div>
</body>
</html>

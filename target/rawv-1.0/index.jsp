<%@page import="java.util.List"%>
<%@page import="com.ads.rawv.dto.Sintesi"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script src="js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="js/fileinput.min.js"></script>
<script src="js/fileinput_locale_it.js"></script>
<link href="css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/rawv.css" media="all" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='css/bootstrap.min.css'>
<script>
	$(document).on('ready', function() {
		$("#input").fileinput({
			uploadUrl : "upload",
			uploadAsync : true,
			showPreview : false,
			maxFileCount : 20
			//allowedFileExtensions : [ 'xls' ]
		});
		$('#input').on('filebatchuploadcomplete', function(event, params) {
			//document.location.href = "sintesi.jsp";
			window.location.reload();
		});
		var tableH=window.innerHeight-200;
		$("#contentTable").css("height", tableH+"px" )
		$("#contentTbody").css("height", (tableH-62)+"px" )
	});
</script>
</head>
<body>
	<div class="container-fluid content">
		<div class="row header" >
			<div class="h1 title">RAW VIEWER</div>
			<input id="input" name="file" type="file" multiple class="file-loading">
		</div>
		<%
			List<Sintesi> sintesi = (List<Sintesi>) session.getAttribute("sintesi");
		%>
		<div class="table-responsive row" id="contentTable">
			<table class="table table-hover">
				<thead>
					<tr class="active">
						<th></th>
						<th>Ore</th>
						<th>Giorni</th>
						<th>Tk</th>
					</tr>
				</thead>
				<tbody id="contentTbody">
					<c:forEach var="item" items="${sintesi}">
						<tr class="success">
							<td colspan="2" style="width: 50%"><a href="dettaglio.jsp?nominativo=${item.nominativo}">${item.nominativo}</a></td>
							<td>${item.mese} (${item.giorniLavorativi})</td>
							<td>${item.ticket}</td>
						</tr>
						<c:set var="total" value="0" />
						<c:forEach var="cdl" items="${item.cdls}">
							<tr>
								<td>${cdl.key}</td>
								<td>${cdl.value}</td>
								<td>${cdl.value/8}</td>
								<td></td>
							</tr>
							<c:set var="total" value="${total + cdl.value}" />
						</c:forEach>
						<tr class="warning">
							<td><span class="bold">Totale:</span></td>
							<td><span class="bold">${total}</span></td>
							<td><span class="bold">${total/8}</span></td>
							<td><span class="bold">${item.ticket}</span></td>
						</tr>
						<tr class="active">
							<td colspan="4" style="width: 100%"></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="row footer">
			<a class="btn btn-default" href="reset.jsp" role="button">Cancella Cache</a>
			<a class="btn btn-default" href="#" role="button" onclick="window.location.reload();">Ricarica</a>
		</div>
	</div>
</body>
</html>

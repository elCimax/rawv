<%@page import="com.ads.rawv.dto.Sintesi.Mese"%>
<%@page import="com.ads.rawv.utils.PropertiesUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<link href="css/rawv.css" media="all" rel="stylesheet" type="text/css" />
		<link rel='stylesheet' href='css/bootstrap.min.css'>
	</head>
	<body>
		<div class="container-fluid content">
			<c:set var="mesi" value="<%=Mese.values()%>"/>
			<form class="form-horizontal" action="adminSave" method="post">
				<c:forEach var="mese" items="${mesi}">
					<div class="form-group">
						<label for="${mese.key}" class="col-sm-3 control-label">${mese}</label>
						<div class="col-sm-1">
						<c:set var="current"  value='<%=PropertiesUtil.getInteger("giorni.lavorativi."+((Mese)pageContext.getAttribute("mese")).getKey(),20)%>'/>
							<select name="${mese.key}" id="${mese.key}" class="form-control">
								<c:forEach varStatus="status" begin="1" end="30">
									<c:set var="selected"  value=""/>
									<c:if test="${current==status.count}">
										<c:set var="selected"  value="selected"/>
									</c:if>
									<option value="${status.count}" ${selected}>${status.count}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</c:forEach>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Salva</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>

package com.ads.rawv.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

import com.ads.rawv.utils.LoggerUtil.Errors;


public class PropertiesUtil  {


	private static CompositeConfiguration config;
	
	private static final String location="config.properties";
	
	
	static LoggerUtil logger=LoggerUtil.getLogger();
	
	static {
		config = new CompositeConfiguration();
		config.addConfiguration(new SystemConfiguration());
		try {
			PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration(location);
			FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
			propertiesConfiguration.setReloadingStrategy(strategy);
			config.addConfiguration(propertiesConfiguration);
		} catch (Exception e) {
			logger.error(Errors.ERR001, "Errore configurazione properties:", e);
		}
		
	}
	
	public static String getProperty(String key) {
		List<Object> values = config.getList(key);
		if (values == null || values.size() < 1) {
			logger.debug("value not found for key %s", key);
			return null;
		}
		String concatValue = "";
		for (Object obj : values) {
			concatValue += "," + obj;
		}
		return concatValue.substring(1);
	}
	
	
	public static Map<String,String> getAllProperties(){
		Map<String,String> properties=new HashMap<String, String>();
		try {
			Iterator<String> keys=config.getKeys();
			logger.debug("Propreties keys: %s",keys);
			while(keys.hasNext()) {
				String key=keys.next();
				String value=getProperty(key);
				logger.info("Proprety %s = %s", key,value);
				properties.put(key, value);
			}
		} catch (Throwable e) {
			logger.warn("can't print properties continue (%s)...", e.getMessage());
		}
		return properties;
	}
	
	
	
	public static String getString(String key,String defaultValue) {
		return config.getString(key, defaultValue);
	}
	
	public static Long getLong(String key,Long defaultValue) {
		return config.getLong(key, defaultValue);
	}
	
	public static Integer getInteger(String key,Integer defaultValue) {
		return config.getInteger(key, defaultValue);
	}

	public static List<String> getList(String key) {
		return Arrays.asList(config.getStringArray(key));
	}

	public static boolean getBoolean(String key, boolean defaulz) {
		return config.getBoolean(key,defaulz);
	}

	public static void setInteger(String key, Integer value) {
		config.setProperty(key, value);
	}
}

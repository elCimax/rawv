package com.ads.rawv.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoggerUtil implements Serializable {

	private static final long serialVersionUID = 1L;


	public enum Errors {
		ERR001
	}

	
	

	private LoggerUtil() {
	}

	public static LoggerUtil getLogger() {
		return new LoggerUtil();
	}

	static public String message(Errors code, String message, Object... params) {
	
		String prefix = "%s - ";
		String error = "";

		if (code != null)
			error = " - "+code + ":";

		List<Object> parList = new ArrayList<Object>();
		parList.add(error);
		if (params != null && params.length > 0)
			parList.addAll(Arrays.asList(params));

		try {
			return String.format(prefix + message,
					parList.toArray(new Object[1]));
		} catch (Throwable e) {
			return prefix + message + "-" + parList;
		}
	}

	public void info(String message, Object... params) {
		System.out.println(message(null, message, params));
	}

	public void trace(String message, Object... params) {
		System.out.println(message(null, message, params));
	}

	public void debug(String message, Object... params) {
		System.out.println(message(null, message, params));
	}


	public void error(Errors code, String message, Throwable e,Object... params) {
		System.out.println(message(code, message, params)); 
		e.printStackTrace();
	}

	public void error(Errors code, String message,Object... params) {
		System.out.println(message(code, message, params));
	}

	public void error(String message, Throwable exception, Object... params) {
		error(null, message, exception, params);
	}

	public void warn(String message, Object... params) {
		System.out.println(message(null, message, params));
	}

	public void verbose(String message, Object... params) {
		System.out.println(message(null, message, params));
	}

	public boolean isInfoEnabled() {
		return true;
	}

	public void error(String message) {
		error(null, message);
	}
}
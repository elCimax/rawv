package com.ads.rawv.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ads.rawv.dto.Sintesi.Mese;
import com.ads.rawv.utils.LoggerUtil;
import com.ads.rawv.utils.PropertiesUtil;

@WebServlet(name = "adminSave", urlPatterns = { "/adminSave" })
public class AdminSave extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	static LoggerUtil logger=LoggerUtil.getLogger();

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
		resp.sendRedirect("admin.jsp");
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
		resp.sendRedirect("admin.jsp");
	}
	
	private void processRequest(HttpServletRequest req, HttpServletResponse resp)  {
		for(Mese mese:Mese.values()){
			if(req.getParameter(mese.getKey())!=null){
				try {
					Integer value=new Integer(req.getParameter(mese.getKey()));
					PropertiesUtil.setInteger("giorni.lavorativi."+mese.getKey(), value);
				} catch (Exception e) {
					logger.warn("set %s for %s but isn't a number",req.getParameter(mese.getKey()),mese);
				}
				
			}
		}
	}

	
	
	
}


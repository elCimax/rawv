package com.ads.rawv.web;

import javax.servlet.annotation.WebServlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.ads.rawv.dto.Giornata;
import com.ads.rawv.dto.Sintesi;
import com.ads.rawv.dto.Sintesi.Mese;
import com.ads.rawv.utils.LoggerUtil;

@WebServlet(name = "upload", urlPatterns = { "/upload" })
@MultipartConfig
public class Uploader extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	static LoggerUtil logger=LoggerUtil.getLogger();

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	
	@SuppressWarnings("unchecked")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

		final Part filePart = request.getPart("file");
		InputStream filecontent = null;
		final PrintWriter writer = response.getWriter();

		try {
			filecontent = filePart.getInputStream();

			List<Giornata> giornate= (List<Giornata>)request.getSession().getAttribute("giornate"); 
			if(giornate==null){	
				synchronized (request.getSession()) {
					giornate=new ArrayList<Giornata>();
					request.getSession().setAttribute("giornate", giornate);
				}
			}
			giornate=readExcel(filecontent,giornate);
			
			List<Sintesi> sintesi=createSintesi(giornate);
			
			request.getSession().setAttribute("sintesi", sintesi);
			request.getSession().setAttribute("giornate", giornate);
			
			writer.println("{\"id\":\"" + UUID.randomUUID() + "\"}");

		} catch (Exception fne) {
			fne.printStackTrace();
			writer.println("{\"error\":\"" + fne.getMessage() + "\"}");
		} finally {
			if (filecontent != null) {
				filecontent.close();
			}
			if (writer != null) {
				writer.close();
			}
		}
	}

	private List<Sintesi> createSintesi(List<Giornata> giornate) throws Exception {
		List<Sintesi> list=new ArrayList<Sintesi>();
		for(Giornata giornata:giornate){
			boolean insert=true;
			Sintesi sintesi=new Sintesi();
			sintesi.setNominativo(giornata.getNominativo());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal= new GregorianCalendar();
			cal.setTime(sdf.parse(giornata.getGiorno()));
			sintesi.setMese(Mese.findMese(cal.get(Calendar.MONTH)));
			
			if(list.contains(sintesi)){
				int index=list.indexOf(sintesi);
				sintesi=list.get(index);
				insert=false;
			}
			if(giornata.getAssenza()==null){
				sintesi.addOre(giornata.getCdl(),giornata.getOre());
			}
			else{
				sintesi.addOre(giornata.getAssenza(),giornata.getOre());
			}
			
			if(giornata.getTicket()!=null){
				sintesi.addTicket(giornata.getTicket());
			}
			
			if(insert){
				list.add(sintesi);
			}
		}
		logger.info("Sintesi: %s", list);
		return list;
	}

	private List<Giornata> readExcel(InputStream file,List<Giornata> giornate) throws Exception {
		HSSFWorkbook workbook = null;
		try {
			workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			if(rowIterator.hasNext())
				rowIterator.next();
			
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Cell giorno = row.getCell(1);
				Cell nominativo = row.getCell(3);
				Cell cdl = row.getCell(4);
				Cell codAssenza = row.getCell(7);
				Cell assenza = row.getCell(8);
				Cell ticket = row.getCell(11);
				Cell ore = row.getCell(15);
				Giornata giornata=new Giornata();
				giornata.setNominativo(nominativo.getStringCellValue());
				giornata.setCdl(cdl.getStringCellValue());
				giornata.setGiorno(convertDate(giorno));
				boolean insert=true;
				if(giornate.contains(giornata)){
					int index=giornate.indexOf(giornata);
					giornata=giornate.get(index);
					insert=false;
				}
				if(assenza.getCellType()!=Cell.CELL_TYPE_BLANK){
					String value=assenza.getStringCellValue();
					if(codAssenza.getCellType()!=Cell.CELL_TYPE_BLANK){
						value+=" ("+codAssenza.getStringCellValue()+")";
					}
					giornata.setAssenza(value);
				}
				
				if(ticket.getCellType()==Cell.CELL_TYPE_BLANK || !ticket.getStringCellValue().equals("TK"))
					giornata.setOre((int)ore.getNumericCellValue());
				else 
					if(ticket.getStringCellValue().equals("TK"))
						giornata.setTicket((int)ore.getNumericCellValue());
				
				if(insert){
					giornate.add(giornata);
				}
			}
			
			logger.info("Giornate: %s", giornate);
			
			return giornate;
		} finally {
			if(workbook!=null)
				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	
	private String convertDate(Cell cell){
		if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC){
        	 if(HSSFDateUtil.isCellDateFormatted(cell)){
         		Date date=HSSFDateUtil.getJavaDate(cell.getNumericCellValue());
         		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
					return sdf.format(date);
				}
				else
					return cell.getNumericCellValue()+"";
             }
        return cell.getStringCellValue();
	}
	
	
	
}

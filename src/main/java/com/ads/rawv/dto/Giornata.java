package com.ads.rawv.dto;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Giornata {
	private String nominativo;
	private String giorno;
	private String cdl;
	private Integer ore;
	private Integer ticket;
	private String assenza;
	public String getNominativo() {
		return nominativo;
	}
	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}
	public String getGiorno() {
		return giorno;
	}
	public void setGiorno(String giorno) {
		this.giorno = giorno;
	}
	public Integer getOre() {
		return ore;
	}
	public void setOre(Integer ore) {
		this.ore = ore;
	}
	public Integer getTicket() {
		return ticket;
	}
	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}
	public String getAssenza() {
		return assenza;
	}
	public void setAssenza(String assenza) {
		this.assenza = assenza;
	}
	
	public String getCdl() {
		return cdl;
	}
	public void setCdl(String cdl) {
		this.cdl = cdl;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdl == null) ? 0 : cdl.hashCode());
		result = prime * result + ((giorno == null) ? 0 : giorno.hashCode());
		result = prime * result + ((nominativo == null) ? 0 : nominativo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Giornata other = (Giornata) obj;
		if (cdl == null) {
			if (other.cdl != null)
				return false;
		} else if (!cdl.equals(other.cdl))
			return false;
		if (giorno == null) {
			if (other.giorno != null)
				return false;
		} else if (!giorno.equals(other.giorno))
			return false;
		if (nominativo == null) {
			if (other.nominativo != null)
				return false;
		} else if (!nominativo.equals(other.nominativo))
			return false;
		return true;
	}
	
	
	public Integer getWeek(){
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal =new GregorianCalendar();
			cal.setTime(sdf.parse(giorno));
			return cal.get(Calendar.WEEK_OF_YEAR)-1;
		} catch (Exception e) {
			return null;
		}
	}
	
	
	@Override
	public String toString() {
		return "Giornata [nominativo=" + nominativo + ", giorno=" + giorno + ", cdl=" + cdl + ", ore=" + ore + ", ticket=" + ticket
				+ ", assenza=" + assenza + "]";
	}

}

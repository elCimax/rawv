package com.ads.rawv.dto;

import java.util.HashMap;
import java.util.Map;

import com.ads.rawv.dto.Sintesi.Mese;
import com.ads.rawv.utils.PropertiesUtil;

public class Sintesi {
	
	public enum Mese{
		Gennaio 	("gennaio"),
	    Febbraio	("febbraio"),
	    Marzo		("marzo"),
	    Aprile		("aprile"),
	    Maggio		("maggio"),
	    Giugno		("giugno"),
	    Luglio  	("luglio"),
	    Agosto  	("agosto"),
	    Settembre	("settembre"),
	    Ottobre		("ottobre"),
	    Novembre	("novembre"),
	    Dicembre	("dicembre");
		
		String key;
		
		Mese(String key){
			this.key=key;
		}
		
		public String getKey(){
			return key;
		}

		public static Mese findMese(int i) {
			for(Mese mese:values()){
				if(mese.ordinal()==i)
					return mese;
			}
			return null;
		}
	}
	
	
	private String nominativo;
	private Mese mese;
	private Map<String,Integer> cdls= new HashMap<String,Integer>();
	private Integer ticket=0;

	public String getNominativo() {
		return nominativo;
	}
	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}
	public Map<String,Integer> getCdls() {
		return cdls;
	}
	public void putCdls(String cdl) {
		if(cdls.get(cdl)==null)
			cdls.put(cdl,0);
	}
	public Integer getTicket() {
		return ticket;
	}
	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nominativo == null) ? 0 : nominativo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sintesi other = (Sintesi) obj;
		if (nominativo == null) {
			if (other.nominativo != null)
				return false;
		} else if (!nominativo.equals(other.nominativo))
			return false;
		if (mese == null) {
			if (other.mese != null)
				return false;
		} else if (!mese.equals(other.mese))
			return false;
		
		return true;
	}
	
	public void addOre(String cdl, Integer ore) {
		Integer oreLavorate=cdls.get(cdl);
		if(oreLavorate==null)
			oreLavorate=0;
		if(ore!=null)
			oreLavorate+=ore;
		cdls.put(cdl,oreLavorate);
	}

	public void addTicket(Integer ticket) {
		if(ticket!=null)
			this.ticket+=ticket;
	}

	@Override
	public String toString() {
		return "Sintesi [nominativo=" + nominativo  + ", mese=" + mese+ ", cdls=" + cdls + ", ticket=" + ticket + "]";
	}
	public Mese getMese() {
		return mese;
	}
	public void setMese(Mese mese) {
		this.mese = mese;
	}
	
	public Integer getGiorniLavorativi() {
		if(mese!=null)
			return PropertiesUtil.getInteger("giorni.lavorativi."+mese.key, 0);
		return 0;
	}
	
}
